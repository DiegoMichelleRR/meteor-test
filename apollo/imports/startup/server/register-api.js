import {createApolloServer} from 'meteor/apollo';
import {makeExecutableSchema} from 'graphql-tools';
import merge from 'lodash/merge';

import ResolutionsSchema from '../../api/resolutions/Resolutions.graphql';
import ResolutionsResolvers from '../../api/resolutions/resolvers';
//               GraphQL Schema
//Queries are functions that return things. (hi returns a String)
//Mutations allow us to modify database
//Resolutions
const typeDefs = [
    `
        type Query {
            hi: String,
            resolutions: [Resolution]
        }
    `,
    ResolutionsSchema
];

// Actual Method or server side code
const testResolvers = {
    Query:{
        hi(){
          return "Hello level up";  
        }
    }
}

const resolvers = merge(testResolvers, ResolutionsResolvers);

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
})

createApolloServer({ schema });