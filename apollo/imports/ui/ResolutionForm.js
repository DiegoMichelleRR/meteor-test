import React from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

//We created a new query called "createResolution"
//then we create a mutation called createResolution
//and at last, we used the createResolution mutation defined in resolvers.js
const createResolution = gql `
    mutation createResolution($name: String!){
        createResolution(name: $name){
            _id
        }
    }
`;

class ResolutionForm extends React.Component {

    submitForm = () => {
        console.log(this.name.value);
        this.props.createResolution({
            variables:{
                name: this.name.value
            }
        }).then(({data}) => {
            // this.props.refetch();
        }).catch(error => {
            console.log(error);
        });
    }

    render(){
        return(
            <div>
                <input type="text" ref={(input) => (this.name = input)}/>
                <button onClick={this.submitForm}>Submit</button>
            </div>
        );
    }
}

//we assign the name to the resolution so we access them within react props with this name
export default graphql(createResolution, {
    name: 'createResolution',
    options: {
        // Here we specify when to refetch data when a mutation happens.
        // This 'Resolutions' is a reference to the query Resolutions in App.js
        // This is all thanks to Apollo
        refetchQueries: [
            'Resolutions'
        ]
    }
})(ResolutionForm)