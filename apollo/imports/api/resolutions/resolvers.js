import Resolutions from './resolutions';

// How to insert data into the DB:
// Resolutions.insert({
//     name: 'Test res insert'
// });

// Actual Method or server side code (like our API endpoint to modify data)

// We need to define these types within Resolutions.graphql

// This is where the app gets the information from. Having a Resolutions.find or
// any other method takes the information from the database to the actual app.
export default{
        Query:{
            resolutions(){
                return Resolutions.find({}).fetch();
            }
        },

        Mutation:{
            createResolution(obj, args, context){
                const resolutionId = Resolutions.insert({
                    name: args.name
                });
                return Resolutions.findOne(resolutionId);
            }
        }
}